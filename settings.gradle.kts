rootProject.name = "demo"

include("client", "service")
project(":client").name = "demo-client"
project(":service").name = "demo-service"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://repository.sofi.com/artifactory/repo/")
    }
}

