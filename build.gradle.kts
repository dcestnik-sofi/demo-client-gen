plugins {
    idea
    kotlin("jvm") version "1.3.72"
    id("com.diffplug.spotless") version "5.1.0"
}

allprojects {
    group = "com.example"
    version = "0.0.1-SNAPSHOT"

    repositories {
        mavenCentral()
        maven("https://repository.sofi.com/artifactory/repo/")
    }

    apply {
        plugin("org.jetbrains.kotlin.jvm")
        plugin("com.diffplug.spotless")
    }

    tasks.test {
        useJUnitPlatform()
    }

    tasks.compileKotlin {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }

    spotless {
        kotlin {
            ktlint("0.38.1")
        }
    }
}

subprojects {
    java.sourceCompatibility = JavaVersion.VERSION_11
    java.targetCompatibility = JavaVersion.VERSION_11
}
